# LXDE SIG

## 简介

LXDE SIG负责维护openKylin系统的LXDE桌面环境及其组件，目前只是代码的搬运工


## SIG成员

### Owner

- [DSOE1024](https://gitee.com/DSOE1024)

### Maintainers

- [DSOE1024](https://gitee.com/DSOE1024)
- [chenchongbiao](https://gitee.com/chenchongbiao)
- [XXTXTOP](https://gitee.com/XXTXTOP)

## 软件包列表

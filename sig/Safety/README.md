# 安全保障SIG（Safety SIG）

安全保障兴趣组（Safety SIG）的主要目标是通过使用GSN（Goal Structuring Notation）技术，使openKylin开源操作系统项目在安全保障方面更加透明、可理解和可控。

## 工作目标

1. 通过明确的安全目标、策略和证据，提升用户对openKylin的信心，同时促进社区内的知识共享。
2. 提高openKylin操作系统在安全性方面的可维护性和可扩展性，以满足不断变化的安全需求和挑战。

## SIG 成员

- 陈泽众（52215902018@stu.ecnu.edu.cn）

## SIG 维护包列表

(建设中)
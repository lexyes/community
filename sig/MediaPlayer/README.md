## 媒体播放器组
### 工作目标
优化openKylin媒体播放体验

## SIG成员
### Owner
- 刘聪（liucong1@kylinos.cn）

### Maintainers
- 刘聪 (liucong1@kylinos.cn)
- 赵民勇 (zhaominyong@kylinos.cn)
- 余烁奇（yushuoqi@kylinos.cn)

## SIG邮件列表
- mediaplayer@list.openkylin.top

## SIG维护包列表
- kylin-video
- kylin-music

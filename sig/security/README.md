# Security SIG

安全防御组，研制操作系统安全防护技术和功能，提供并维护操作系统安全防御机制。针对目前openKylin开源版本中的安全功能、安全组件等软件包进行维护和cve修复；并结合目前Linux平台上的一些安全技术进行研究、开源安全软件进行维护和研制、Linux系统中的安全加固方案进行工具化设计和研制。具体包括如下方面：

- 在openKylin社区版本中使能主流的Linux安全特性，提供系统安全工具、库、基础设施等能力。
- 改善现有安全技术的应用体验，帮助安全创造实际的价值。
- 探讨新的安全防御技术。


## 邮件列表

security@lists.openkylin.top

## SIG成员
### Owner
- [杨诏钧](https://gitee.com/andrew)

### Maintainers
- [谢芳](https://gitee.com/xiefang85)
- [王玉成](https://gitee.com/gerrywong6)
- [姬一文](https://gitee.com/jiyiwen)
- [杨钊](https://gitee.com/Sagittarius-yz)
- [岳佳圆](https://gitee.com/yuejiayuan)

### Committers
- [于博](https://gitee.com/kylinyubo)
- [田虎军](https://gitee.com/tianhujun)
- [康昱](https://gitee.com/kangyu520)
- [付焕章](https://gitee.com/fluyhz)
- [孟圆](https://gitee.com/kylinmy)
- [蒋杏松](https://gitee.com/jiangxingsong)
- [甘建庆](https://gitee.com/ganjianqing)
- [边秀宁](https://gitee.com/bian-xiuning)
- [宋俊涛](https://gitee.com/corwin-song)
- [谢宇星](https://gitee.com/xieyuxing168)

## SIG维护包列表
- [libselinux](https://gitee.com/openkylin/libselinux)
- [libsemanage](https://gitee.com/openkylin/libsemanage)
- [libsepol](https://gitee.com/openkylin/libsepol)
- [apparmor](https://gitee.com/openkylin/apparmor)
- [audit](https://gitee.com/openkylin/audit)
- [pam](https://gitee.com/openkylin/pam)
- [libpwquality](https://gitee.com/openkylin/libpwquality)
- [adduser](https://gitee.com/openkylin/adduser)
- [shadow](https://gitee.com/openkylin/shadow)
- [cracklib2](https://gitee.com/openkylin/cracklib2)
- [libkylin-chkname](https://gitee.com/openkylin/libkylin-chkname)
- [iptables](https://gitee.com/openkylin/iptables)
- [nftables](https://gitee.com/openkylin/nftables)
- [libnftnl](https://gitee.com/openkylin/libnftnl)
- [libmnl](https://gitee.com/openkylin/libmnl)
- [libbpf](https://gitee.com/openkylin/libbpf)
- [openssl](https://gitee.com/openkylin/openssl)
- [openssl3](https://gitee.com/openkylin/openssl3)
- [gnutls28](https://gitee.com/openkylin/gnutls28)
- [libxcrypt](https://gitee.com/openkylin/libxcrypt)

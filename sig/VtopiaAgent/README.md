

## VtopiaAgent兴趣小组（SIG）

VtopiaAgent SIG专注于软件物料信息的收集、分析以及这些数据输送的公共代理。期望与社区合作打造软件物料信息等标准数据格式，打造公共代理工具提供标准数据接口。

## 工作目标

- 负责提供软件物料信息以及一些其它信息收集的能力。
- 负责打造数据输出的公共代理工具，用于收集信息输出的代理以及检测组件的管理。
- 负责软件物料信息等标准数据格式以及提供标准数据接口。

## SIG成员

## SIG members
### Owner
- Yunzhang Sun(yunzhang@vulab.com.cn)

### Maintainers
- Yunzhang Sun(yunzhang@vulab.com.cn)

## Email


## SIG维护包列表


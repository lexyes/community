# 兼容能力 SIG

本SIG组致力于负责openKylin社区软硬件生态兼容性探索，包括整机、外设、应用软件兼容适配，社区衍生发行版兼容测评等相关能力探索。

## SIG 职责和目标

- 与硬件（整机、外设）兼容性，成立相关兼容性测试项目，进行操作系统与硬件兼容性的相关方面探索活动。
- 操作系统与应用程序兼容性策略探索、关键影响因素，类似白名单机制制定。
- 识别POSIX/lsb等组织对于兼容性的定义及规范性满足度审查。
- 社区衍生发行版兼容认证测评支持以及自动化兼容测评工具维护。(https://gitee.com/openkylin/community/blob/master/sig/compatibility/openKylin%E8%A1%8D%E7%94%9F%E5%8F%91%E8%A1%8C%E7%89%88%E5%8F%91%E8%A1%8C%E6%8C%87%E5%8D%97.md)

## SIG 成员
### Owner
- James_2016（马发俊）
- jlspcdd（曹丹丹）


### Maintainers
- James_2016(mafajun@kylinos.cn)
-jlspcdd（caodandan@kylinos.cn）
- Feng Peipei(fengpeipei@kylinos.cn) 
- yexuefanghua（fanjiao@kylinos.cn）
- Judyzhu741011(zhuxiaohong@kylinos.cn)
- kang_yan_hong（kangyanhong@kylinos.cn）

## 邮件列表
compatibility@lists.openkylin.top

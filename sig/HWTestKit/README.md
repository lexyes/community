 **HWTestKit SIG** 

致力于openKylin社区版本的整机和硬件兼容自动化工具开发和探索,应用自动化相关新技术并推进自动化工具在社区维护，提升openKylin社区版本质量，包括社区版本测试、质量保障。

HWTestKit主要涉及的质量保障内容：

开发测试工具以提升代码开发效率和测试验证效率

 **SIG成员** 

 **SIG-owner** 

唐晓东

 **SIG-maintainer** 

chutiexin(chutiexin@kylinos.cn)

wangyibo(wangyibo@kylinos.cn)

huwencan(huwencan@kylinos.cn)

liuqinglin(liuqinglin@kylinos.cn)

 **邮件** 

hwtestkit@lists.openkylin.top

 **SIG维护包列表** 

kylinpvt-o

openkylin-kcc

